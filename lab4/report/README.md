#LV 4


##Problem 1

###Gaussian
$`  \sigma = 1  `$
![1](problem_1\Gauss_1.png)
![2](problem_1\GaussHistogram_1.png)

$`  \sigma = 5  `$
![3](problem_1\Gauss_5.png)
![4](problem_1\GaussHistogram_5.png)

$`  \sigma = 10  `$
![5](problem_1\Gauss_10.png)
![6](problem_1\GaussHistogram_10.png)

$`  \sigma = 20  `$
![7](problem_1\Gauss_20.png)
![8](problem_1\GaussHistogram_20.png)

$`  \sigma = 30  `$
![9](problem_1\Gauss_30png)
![10](problem_1\GaussHistogram_30.png)

$`  \sigma = 40  `$
![11](problem_1\Gauss_40.png)
![12](problem_1\GaussHistogram_40.png)

$`  \sigma = 60  `$
![13](problem_1\Gauss_60.png)
![14](problem_1\GaussHistogram_60.png)


###Uniform
$` (-20, 20) `$
![1](problem_1\Uniform_-20_20.png)
![2](problem_1\UniformHistogram_-20_20.png)

$` (-40, 40) `$
![3](problem_1\Uniform_-40_40.png)
![4](problem_1\UniformHistogram_-40_40.png)

$` (-60, 60) `$
![5](problem_1\Uniform_-60_60.png)
![6](problem_1\UniformHistogram_-60_60.png)


###Salt and Pepper
$` 5% `$
![1](problem_1\SnP_5.png)
![2](problem_1\SnPHistogram_5.png)

$` 10% `$
![1](problem_1\SnP_10.png)
![2](problem_1\SnPHistogram_10.png)

$` 15% `$
![1](problem_1\SnP_15.png)
![2](problem_1\SnPHistogram_15.png)

$` 20% `$
![1](problem_1\SnP_20.png)
![2](problem_1\SnPHistogram_20.png)




##Problem 2
