import numpy as np
import cv2

def show(img):
  cv2.imshow("title", img)
  cv2.waitKey(0)
  cv2.destroyAllWindows()

img = cv2.imread("../../../images/lenna.bmp", 0)

show(img)

blur = cv2.GaussianBlur(img, (5, 5), 1)

show(blur)

median = cv2.medianBlur(img, 5)

show(median)