import numpy as np
import cv2
import matplotlib.pyplot as plt

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def uniform_noise(img, low, high):
  out = img.astype(np.float32)
  noise = np.random.uniform(low,high, img.shape)
  out = out + noise
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)


def show(img):
  cv2.imshow("title", img)
  cv2.waitKey(0)
  cv2.destroyAllWindows()
def showhist(img):
  hist,bins = np.histogram(img.flatten(), bins=256, range=(0,255))
  plt.vlines(np.arange(len(hist)), 0, hist)
  plt.title("Histogram")
  plt.show()
def savehist(img, name):
  hist,bins = np.histogram(img.flatten(), bins=256, range=(0,255))
  plt.vlines(np.arange(len(hist)), 0, hist)
  plt.title("Histogram")
  plt.savefig(name + ".png")


img = cv2.imread("../../../images/lenna.bmp", 0)


# show(img)
# showhist(img)

# show(salt_n_pepper_noise(img,20))
# showhist(salt_n_pepper_noise(img, 20))

# show(gaussian_noise(img, 0, 12))
# showhist(gaussian_noise(img, 0, 12))

# show(uniform_noise(img, -30, 30))
# showhist(uniform_noise(img, -30, 30))

gauss = [1, 5, 10, 20, 40, 60]
uni= [(-20, 20), (-40, 40), (-60, 60)]
snp = [5, 10, 15, 20]

for i in gauss:
    cv2.imwrite("Gauss_" + str(i) + ".png", gaussian_noise(img, 0, i))
    savehist(gaussian_noise(img, 0, i), "GaussHistogram_" + str(i))

for i in uni:
    cv2.imwrite("Uniform_" + str(i[0]) + "_" + str(i[1]) + ".png", uniform_noise(img, i[0], i[1]))
    savehist(uniform_noise(img, i[0], i[1]), "UniformHistogram_" + str(i[0]) + "_" + str(i[1]))

for i in snp:
    cv2.imwrite("SnP_" + str(i) + ".png", salt_n_pepper_noise(img, i))
    savehist(salt_n_pepper_noise(img, i), "SnPHistogram_" + str(i))
