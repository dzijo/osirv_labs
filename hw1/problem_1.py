import functions

images, names = functions.load_images(0)

for name, image in zip(names, images):
    vertical = image.copy()
    vertical[:, ::2, :] = 0
    functions.save_image("problem_1", vertical, name, "_V")

    horizontal = image.copy()
    horizontal[::2, :, :] = 0
    functions.save_image("problem_1", horizontal, name, "_H")