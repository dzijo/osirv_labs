# Author: Matej Džijan

The program performs similar processes as Problem 7, the only difference being here it's done only on two channels of the `YCrCb` image.

The program first converts an image to `YCrCb` color space and then does subsampling and enlargement of the 2nd (`Cr`) and 3rd (`Cb`) channel.

After that, the image is converted back into `BGR` color space.

```python
    ycrcb = cv2.cvtColor(image, cv2.COLOR_BGR2YCR_CB)
    print(ycrcb.shape)
    temp = []
    for ch in [1, 2]:
        temp.append(enlarge(subsample(ycrcb[:, :, ch], f), f))
    new = np.zeros((temp[0].shape[0], temp[0].shape[1], 3), dtype = np.uint8)
    new[:, :, 0] = crop(ycrcb[:, :, 0], f)
    new[:, :, 1] = temp[0]
    new[:, :, 2] = temp[1]
    bgr = cv2.cvtColor(new, cv2.COLOR_YCR_CB2BGR)
```

### Original image
![Original](../../images/pepper.bmp)

### Subsampled images
`f = 2`
![sub2](pepper_sub2.png)

`f = 4`
![sub4](pepper_sub4.png)

`f = 8`
![sub8](pepper_sub8.png)

`f = 16`
![sub16](pepper_sub16.png)

The results seem to be very similar to the original image with smaller values of `f`, while at higher values of `f`, rectangles on the edges of objects are clearly visible. 
In my opinion, subsampled image with `f = 4` would be an adequate compressed image.

The image would still require the same amount of space for `Y` channel, `size * 8 bits`, but it would require `f` times less space for the `Cr` and `Cb` channel, `(size / f) * 8 bits`.

