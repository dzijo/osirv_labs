# Author: Matej Džijan

The program adds checkerboard pattern of inverted pixels of set width to an image.

It is done in a similar way to the Problem 3, only difference being "black" pixels are not set to black, but to inverted value of that pixel in the original image.

```python
def checkCheckerboard(b, c, width):
    return ((((c % (2*width)) >= width) and ((b % (2*width)) < width)) or (((c % (2*width)) < width) and ((b % (2*width)) >= width)))

def invCheckerboard(names, images, width):
    for name, image in zip(names, images):
        cpy = image.copy()
        f = np.array(range(cpy.shape[0]))
        g = np.array(range(cpy.shape[1]))
        cpy = np.array([[[(255 - d) if checkCheckerboard(b, c, width) else d for c, d in zip(g, a)] for a, b in zip(cpy[:, :, ch], f)] for ch in range(3)], np.uint8)
        cpy = np.transpose(cpy, (1, 2, 0))
        functions.save_image(folder, cpy, name, "_CHINV")

```

### Original image
![Original](../../images/pepper.bmp)

### Checkered image
![Checkered](pepper_CHINV.png)