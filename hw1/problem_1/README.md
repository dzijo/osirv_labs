# Author: Matej Džijan

The program adds black stripes to every other column/row of an image.

This is done with array slicing: `vertical[:, ::2, :] = 0` for columns and `horizontal[::2, :, :] = 0` for rows.

```python
for name, image in zip(names, images):
    vertical = image.copy()
    vertical[:, ::2, :] = 0
    functions.save_image("problem_1", vertical, name, "_V")

    horizontal = image.copy()
    horizontal[::2, :, :] = 0
    functions.save_image("problem_1", horizontal, name, "_H")
```

### Original image
![Original](../../images/pepper.bmp)

### Vertical stripes
![Vertical](pepper_V.png)

### Horizontal stripes
![Horizontal](pepper_H.png)