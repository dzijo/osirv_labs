# Author: Matej Džijan

The program performs Floyd-Steinberg Dithering to an image.
The program goes through every pixel of an image quantising it using the formula:

![quantize]( http://latex.codecogs.com/gif.latex?u_%7Bi%2C%20j%7D%20%3A%3D%20%5Cleft%20%28%20%5Cleft%20%5Clfloor%20%5Cfrac%7B%20u_%7Bi%2Cj%7D%20%7D%7Bd%7D%20%5Cright%20%5Crfloor%20&plus;%20%5Cfrac%7B1%7D%7B2%7D%20%5Cright%20%29%20*%20d)

where $` d = 2^{8-q} `$.

Error is then calculated using `e = cpy[i, j] - quantised`.

The neighbours are then changed as given by:

$` u_{i+1,j} = u_{i+1,j} + \frac{7}{16} \times e `$

$` u_{i-1,j+1} = u_{i-1,j+1} + \frac{3}{16} \times e `$

$` u_{i,j+1} = u_{i,j+1} + \frac{5}{16} \times e `$

$` u_{i+1,j+1} = u_{i+1,j+1} + \frac{1}{16} \times e `$

Before chaning the neighbours, the program checks if the pixel is at the edge and adjusts accordingly.

```python
def fsDithering(image, q):
    cpy = image.copy().astype(np.float)
    d = pow(2.0, 8.0 - q)
    X = range(cpy.shape[0])
    Y = range(cpy.shape[1])
    print(len(X), len(Y), len(cpy), len(cpy[0]))
    for i in X:
        for j in Y:
            quantised = (np.floor(cpy[i][j] / d) + 1/2.0) * d
            e = cpy[i, j] - quantised
            cpy[i, j] = quantised

            if ((i + 1) == len(X)) or ((j + 1) == len(Y)):
                if (j + 1) != len(Y):
                    cpy[i-1][j+1] += 3.0/16.0 * e
                    cpy[i][j+1] += 5.0/16.0 * e
                    continue
                if (i + 1) != len(X):
                    cpy[i+1][j] += 7.0/16.0 * e
                    continue
                continue
            else:
                cpy[i+1][j] += 7.0/16.0 * e
                cpy[i+1][j+1] += 1.0/16.0 * e
                cpy[i-1][j+1] += 3.0/16.0 * e
                cpy[i][j+1] += 5.0/16.0 * e
    return cpy.astype(np.uint8)
```

### Original image
![Original](../../images/boats.bmp)

### Quantisation using Floyd-Steinberg Dithering
`q = 1`
![q1](boats_1fsd.png)

`q = 2`
![q2](boats_2fsd.png)

`q = 3`
![q3](boats_3fsd.png)

### Normal quantisation
`q = 1`
![q11](../../lab3/report/problem5/boats_1.bmp)

`q = 2`
![q12](../../lab3/report/problem5/boats_2.bmp)

`q = 3`
![q13](../../lab3/report/problem5/boats_3.bmp)

Quantisation using Floyd-Steinberg Dithering seems to produce images of higher quality than normal quantisation at higher values of `q`.
At lower values of `q`, they both have different problems, but normal quantisation seems to produce results less similar to original image. 