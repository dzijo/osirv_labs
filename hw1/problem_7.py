import numpy as np
import cv2
import os

def crop(image, f):
    w = int(image.shape[0] / f) * f
    h = int(image.shape[1] / f) * f
    return image[0:w, 0:h]

def subsample(image, f):
    out = np.zeros_like(image)
    out = crop(out, f)
    g = 1.0 / (f**2)
    X = int(out.shape[0] / f)
    Y = int(out.shape[1] / f)
    out = out[0:X, 0:Y]

    for i in range(X):
        for j in range(Y):
            out[i, j] = g * np.sum(image[i * f:i * f + f, j * f:j * f + f])

    return out.astype(np.uint8)

def enlarge(imagech, f):
    h = imagech.shape[0]
    w = imagech.shape[1]
    out = np.zeros((h * f, w * f))

    for i in range(h):
        for j in range(w):
            out[i * f: i * f + f, j * f: j * f + f] = imagech[i, j]

    return out.astype(np.uint8)



names = ["baboon", "pepper"]
images =[cv2.imread("C:/Fakultet/OSIRV/osirv_labs/images/baboon.bmp"), cv2.imread("C:/Fakultet/OSIRV/osirv_labs/images/pepper.bmp")]
fs = [2, 4, 8, 16]

for name, image in zip(names, images):
    for f in fs:
        ycrcb = cv2.cvtColor(image, cv2.COLOR_BGR2YCR_CB)
        #print(ycrcb.shape)
        temp = []
        for ch in [1, 2]:
            temp.append(enlarge(subsample(ycrcb[:, :, ch], f), f))
        new = np.zeros((temp[0].shape[0], temp[0].shape[1], 3), dtype = np.uint8)
        new[:, :, 0] = crop(ycrcb[:, :, 0], f)
        new[:, :, 1] = temp[0]
        new[:, :, 2] = temp[1]
        bgr = cv2.cvtColor(new, cv2.COLOR_YCR_CB2BGR)

        cv2.imwrite(os.path.join("C:/Fakultet/OSIRV/osirv_labs/hw1/problem_7", name + "_sub" + str(f) + ".png"), bgr)
