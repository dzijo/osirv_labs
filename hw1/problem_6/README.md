# Author: Matej Džijan

The program subsamples an image using an average of each `f x f` segment of the image, thus lowering the resolution of the image and returns an image of similar resolution but lower actual quality.

`new = enlarge(subsample(image, f), f)`

This is done by first subsampling the image and then enlarging it close to its original size.

The image first needs to be cropped into shape where both axes are divisible by `f`.

Then the value of each pixel is calculated using the formula:

$` res\left( i,j \right) = \frac{1}{f^2} \times \sum_{p=i*f}^{i*f+f} \sum_{q=j*f}^{j*f+f} orig(p,q) `$

where $` res\left( i,j \right) `$ is `out[i, j]` and $` \sum_{p=i*f}^{i*f+f} \sum_{q=j*f}^{j*f+f} orig(p,q) `$ is `np.sum(image[i * f:i * f + f, j * f:j * f + f])`
`out` is then outputted as array of type `np.uint8`.
After subsampling, the image needs to be enlarged close to its original size.

This is done by iterating through every pixel of `out` image which is larger than the subsampled image `f` times.

`out[i * f: i * f + f, j * f: j * f + f] = image[i, j]`
Where `image` is the subsampled image which is the output of `subsample(image, f)` function.

For images in color, this is done separately for each channel.

```python
def crop(image, f):
    w = int(image.shape[0] / f) * f
    h = int(image.shape[1] / f) * f
    return image[0:w, 0:h]

def subsample(image, f):
    out = np.zeros_like(image)
    out = crop(out, f)
    g = 1.0 / (f**2)
    X = int(out.shape[0] / f)
    Y = int(out.shape[1] / f)
    out = out[0:X, 0:Y]

    for i in range(X):
        for j in range(Y):
            out[i, j] = g * np.sum(image[i * f:i * f + f, j * f:j * f + f])

    return out.astype(np.uint8)

def enlarge(image, f):
    h = image.shape[0]
    w = image.shape[1]
    out = np.zeros((h * f, w * f))

    for i in range(h):
        for j in range(w):
            out[i * f: i * f + f, j * f: j * f + f] = image[i, j]

    return out.astype(np.uint8)
```

### Original image
![Original](../../images/pepper.bmp)

### Subsampled images
`f = 8`
![sub8](pepper_sub8.png)

`f = 16`
![sub16](pepper_sub16.png)

`f = 32`
![sub32](pepper_sub32.png)
