# Author: Matej Džijan

The program adds checkerboard pattern of black pixels of set width to an image.

`cpy = np.array([[[0 if checkCheckerboard(b, c, width) else d for c, d in zip(g, a)] for a, b in zip(cpy[:, :, ch], f)] for ch in range(3)], np.uint8)`

It is done in a similar way to the Problem 2. Difference being now it's iterated through all 3 channels with `ch`. To check if a pixel should be blackened is done with the function `checkCheckerboard(b, c, width)` which checks if a pixel is in top right or bottom left corner of a `width * width` square or not. 

`cpy` is then transposed as its shape should be `(X, Y, 3)`


```python
def checkCheckerboard(b, c, width):
    return ((((c % (2*width)) >= width) and ((b % (2*width)) < width)) or (((c % (2*width)) < width) and ((b % (2*width)) >= width)))

def invCheckerboard(names, images, width):
    for name, image in zip(names, images):
        cpy = image.copy()
        f = np.array(range(cpy.shape[0]))
        g = np.array(range(cpy.shape[1]))
        cpy = np.array([[[0 if checkCheckerboard(b, c, width) else d for c, d in zip(g, a)] for a, b in zip(cpy[:, :, ch], f)] for ch in range(3)], np.uint8)
        cpy = np.transpose(cpy, (1, 2, 0))
        functions.save_image(folder, cpy, name, "_CH")
```

### Original image
![Original](../../images/pepper.bmp)

### Checkered image
![Checkered](pepper_CH.png)