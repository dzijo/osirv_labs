import functions
import numpy as np

images, names = functions.load_images(0)
widths = [8, 16, 32]

img = images[0]

folder = __file__.split('.')[0]

def lines(names, images, widths, horizontal = False):
    for name, image in zip(names, images):
        for i in widths:
            cpy = image.copy()
            if (horizontal):
                cpy = np.rot90(cpy, 1)
            img0 = cpy[:, :, 0]
            img1 = cpy[:, :, 1]
            img2 = cpy[:, :, 2]
            img20 = np.array([[0 if (c % (2*i)) < i else d for c, d in zip(range(len(img0[0])), a)] for a in img0], np.uint8)
            img21 = np.array([[0 if (c % (2*i)) < i else d for c, d in zip(range(len(img1[0])), a)] for a in img1], np.uint8)
            img22 = np.array([[0 if (c % (2*i)) < i else d for c, d in zip(range(len(img2[0])), a)] for a in img2], np.uint8)
            cpy[:, :, 0] = img20
            cpy[:, :, 1] = img21
            cpy[:, :, 2] = img22
            #functions.show(cpy)

            if (horizontal):
                cpy = np.rot90(cpy, 3)
                functions.save_image(folder, cpy, name, "_H" + str(i))
            else:
                functions.save_image(folder, cpy, name, "_V" + str(i))

lines(names, images, widths)
lines(names, images, widths, True)