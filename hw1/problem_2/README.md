# Author: Matej Džijan

The program adds fat black stripes to an image.
This is done with array slicing. It is done separately for each channel of an image.

`img20 = np.array([[0 if (c % (2*i)) < i else d for c, d in zip(range(len(img0[0])), a)] for a in img0], np.uint8)`

Where `a` is one line of the image and is iterated through the whole image. `c` and `d` are position of the pixel and value of the pixel, respectively, which are iterated through the whole line `a`. It sets the value of the pixel to `0` if it's in the first half of a `2 * widht` section, otherwise it leaves it as it is.

Additionaly, the image is rotated twice, once before adding stripes, once after, when adding horizontal stripes.


```python
def lines(names, images, widths, horizontal = False):
    for name, image in zip(names, images):
        for i in widths:
            cpy = image.copy()
            if (horizontal):
                cpy = np.rot90(cpy, 1)
            img0 = cpy[:, :, 0]
            img1 = cpy[:, :, 1]
            img2 = cpy[:, :, 2]
            img20 = np.array([[0 if (c % (2*i)) < i else d for c, d in zip(range(len(img0[0])), a)] for a in img0], np.uint8)
            img21 = np.array([[0 if (c % (2*i)) < i else d for c, d in zip(range(len(img1[0])), a)] for a in img1], np.uint8)
            img22 = np.array([[0 if (c % (2*i)) < i else d for c, d in zip(range(len(img2[0])), a)] for a in img2], np.uint8)
            cpy[:, :, 0] = img20
            cpy[:, :, 1] = img21
            cpy[:, :, 2] = img22

            if (horizontal):
                cpy = np.rot90(cpy, 3)
                functions.save_image(folder, cpy, name, "_H" + str(i))
            else:
                functions.save_image(folder, cpy, name, "_V" + str(i))
```

### Original image
![Original](../../images/pepper.bmp)

### Vertical stripes
`width = 8`
![Vertical8](pepper_V8.png)

`width = 16`
![Vertical16](pepper_V16.png)

`width = 32`
![Vertical32](pepper_V32.png)

### Horizontal stripes
`width = 8`
![Horizontal8](pepper_H8.png)

`width = 16`
![Horizontal16](pepper_H16.png)

`width = 32`
![Horizontal32](pepper_H32.png)