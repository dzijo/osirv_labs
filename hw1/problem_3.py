import functions
import numpy as np
import cv2

images, names = functions.load_images(0)

img = images[5]

folder = __file__.split('.')[0]

def checkCheckerboard(b, c, width):
    return ((((c % (2*width)) >= width) and ((b % (2*width)) < width)) or (((c % (2*width)) < width) and ((b % (2*width)) >= width)))

def invCheckerboard(names, images, width):
    for name, image in zip(names, images):
        cpy = image.copy()
        f = np.array(range(cpy.shape[0]))
        g = np.array(range(cpy.shape[1]))
        #print(len(f), len(g))
        cpy = np.array([[[0 if checkCheckerboard(b, c, width) else d for c, d in zip(g, a)] for a, b in zip(cpy[:, :, ch], f)] for ch in range(3)], np.uint8)
        cpy = np.transpose(cpy, (1, 2, 0))
        functions.save_image(folder, cpy, name, "_CH")

invCheckerboard(names, images, 32)