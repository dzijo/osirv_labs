import numpy as np
import cv2
import os

def crop(image, f):
    w = int(image.shape[0] / f) * f
    h = int(image.shape[1] / f) * f
    return image[0:w, 0:h]

def subsample(image, f):
    out = np.zeros_like(image)
    out = crop(out, f)
    g = 1.0 / (f**2)
    X = int(out.shape[0] / f)
    Y = int(out.shape[1] / f)
    out = out[0:X, 0:Y]

    for i in range(X):
        for j in range(Y):
            out[i, j] = g * np.sum(image[i * f:i * f + f, j * f:j * f + f])

    return out.astype(np.uint8)

def enlarge(image, f):
    h = image.shape[0]
    w = image.shape[1]
    out = np.zeros((h * f, w * f))

    for i in range(h):
        for j in range(w):
            out[i * f: i * f + f, j * f: j * f + f] = image[i, j]

    return out.astype(np.uint8)



names = ["baboon", "pepper"]
images =[cv2.imread("C:/Fakultet/OSIRV/osirv_labs/images/baboon.bmp"), cv2.imread("C:/Fakultet/OSIRV/osirv_labs/images/pepper.bmp")]
fs = [8, 16, 32]

for name, image in zip(names, images):
    for f in fs:
        if len(image.shape) == 2:
            #this is grayscale image
            new = enlarge(subsample(image, f), f)
        elif len(image.shape) == 3:
            #this is color image
            temp = []
            for ch in range(3):
                temp.append(enlarge(subsample(image[:, :, ch], f), f))
            new = np.zeros((temp[0].shape[0], temp[0].shape[1], 3), dtype = np.uint8)
            new[:, :, 0] = temp[0]
            new[:, :, 1] = temp[1]
            new[:, :, 2] = temp[2]
        cv2.imwrite(os.path.join("C:/Fakultet/OSIRV/osirv_labs/hw1/problem_6", name + "_sub" + str(f) + ".png"), new)
