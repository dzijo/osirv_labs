import functions
import numpy as np

images, names = functions.load_images()

folder = __file__.split('.')[0]

def fsDithering(image, q):
    cpy = image.copy().astype(np.float)
    d = pow(2.0, 8.0 - q)
    X = range(cpy.shape[0])
    Y = range(cpy.shape[1])
    print(len(X), len(Y), len(cpy), len(cpy[0]))
    for i in X:
        for j in Y:
            quantised = (np.floor(cpy[i][j] / d) + 1/2.0) * d
            e = cpy[i, j] - quantised
            cpy[i, j] = quantised

            if ((i + 1) == len(X)) or ((j + 1) == len(Y)):
                #print("G")
                #print(i, j)
                if (j + 1) != len(Y):
                    cpy[i-1][j+1] += 3.0/16.0 * e
                    cpy[i][j+1] += 5.0/16.0 * e
                    continue
                if (i + 1) != len(X):
                    cpy[i+1][j] += 7.0/16.0 * e
                    continue
                continue
            else:
                #print("F", i, j)
                cpy[i+1][j] += 7.0/16.0 * e
                cpy[i+1][j+1] += 1.0/16.0 * e
                cpy[i-1][j+1] += 3.0/16.0 * e
                cpy[i][j+1] += 5.0/16.0 * e
    return cpy.astype(np.uint8)

qs = [1, 2, 3]

for name, image in zip(names, images):
    for q in qs:
        img = fsDithering(image, q)
        functions.save_image(folder, img, name, "_" + str(q) + "fsd")