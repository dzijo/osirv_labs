import numpy as np 
import cv2

def show(name, img):
    cv2.imshow(name, img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

slika = cv2.imread('../images/baboon.bmp', cv2.IMREAD_COLOR)
#print(slika.shape)
#print(slika.dtype)

nova = slika.astype(np.float64)
#print(nova.dtype)
#print(nova[0:10:2,0:10:2, 0])

nova[::8, :] = 0
nova[:, ::8] = 0

nova[nova>255] = 255

outimg = nova.astype(np.uint8)

show("Majmun", outimg)

cv2.imwrite("test.png", outimg)