# Labosi 2.0

## Zadatak 1

Crvena slika:

![Crvena slika](crvena.jpg)

Zelena slika:

![Zelena slika](zelena.jpg)

Plava slika:

![Plava slika](plava.jpg)

## Zadatak 2

## Zadatak 3

Slika s obrubom:

![Obrub](Obrub.jpg)

## Zadatak 4

Vertikalno:

![Vertikalno](Suzeni avijon.png)

Horizontalno:

![Horizontalno](Spljosteni avijon.png)

Oboje:

![Oboje](Stisnuti avijon.png)

