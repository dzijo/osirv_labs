# Napisati program koji ce ucitati sliku i napraviti oko ucitane slike obrub debljine 10 piksela. Takvu sliku spremiti.

def show(name, img):
    cv2.imshow(name, img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

import numpy as np 
import cv2

slika = cv2.imread('../images/airplane.bmp')

slika[:10,:] = 0
slika[-11:-1,:] = 0
slika[:,:10] = 0
slika[:,-11:-1] = 0

cv2.imwrite("Obrub.jpg", slika)
show("F", slika)