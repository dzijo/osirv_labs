# Napisati program koji ce ucitati sliku, te iz te slike napraviti tri slike. 
# Prva ce imati dvostruko manje vertikalnih piksela od originala (redova), druga ce imati dvostruko manje horizontalnih piksela od originala (stupaca), 
# a treca ce imati dvostruko manje i jednih i drugih. (Hint: uzimate svaki drugi piksel)

def show(name, img):
    cv2.imshow(name, img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

import numpy as np 
import cv2

slika = cv2.imread('../images/airplane.bmp')

prva = slika[:, ::2].copy()
druga = slika[::2, :].copy()
treca = slika[::2, ::2].copy()

cv2.imwrite("Suzeni avijon.png", prva)
cv2.imwrite("Spljosteni avijon.png", druga)
cv2.imwrite("Stisnuti avijon.png", treca)

show("F", prva)
show("F", druga)
show("F", treca)