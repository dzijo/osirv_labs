#Isti zadatak kao 1., ali kanale prikazati na ekranu koristeci imshow().

def show(name, img):
    cv2.imshow(name, img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

import numpy as np 
import cv2

slika = cv2.imread('../images/airplane.bmp')

crvena = slika.copy()
crvena[:,:,0:2] = 0

zelena = slika.copy()
zelena[:,:,::2] = 0

plava = slika.copy()
plava[:,:,1:3] = 0

show("Crvena!", crvena)
show("Zelena!", zelena)
show("Plava!", plava)