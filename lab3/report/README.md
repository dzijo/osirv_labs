# LV3

## Problem 1

Spajanje 3 slike s hstack.
```
imf = np.hstack((im1, im2, im3))
```
![](problem1/image.png)
Dodavanje okvira.
```
def addBorder(n):
    h, w, c  = im.shape
    outim = np.zeros((h + 2 * n, w + 2 * n, 3), np.uint8)
    outim[n : w + n, n : h + n, : ] = im
    return outim
```
![](problem1/image2.png)

## Problem 2

Konvolucija.
Primjer koda za jedan kernel:
```
kernel2 = np.array([[0, 1, 0], [1, -4, 1], [0, 1, 0]], dtype = float)
im2 = convolve(im, kernel2)
cv2.imwrite('kernel2.png', im2)
```
Rezultati pojedinih kernela:
### Indetity
![](problem2/kernel0.png)

### Edge Detection
![](problem2/kernel1.png)
![](problem2/kernel2.png)
![](problem2/kernel3.png)

### Sharpen
![](problem2/kernel4.png)

### Box blur
![](problem2/kernel5.png)

### Gaussian blur 3 x 3
![](problem2/kernel6.png)

### Gaussian blur 5 x 5
![](problem2/kernel7.png)

### Unsharp masking 5 x 5
![](problem2/kernel8.png)