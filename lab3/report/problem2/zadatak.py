import numpy as np
import cv2

def convolve(image, kernel):
    output = np.zeros( (image.shape[0] -kernel.shape[0] + 1,
                        image.shape[1] - kernel.shape[1] +1) )
    kernel_rev = kernel[::-1,::-1]


    for i in range(0, output.shape[0]):
        for j in range(0, output.shape[1]):
            for k in range(kernel.shape[0]):
                for l in range(kernel.shape[1]):
                    output[i,j] += image[i+k,j+l] * kernel_rev[k,l]

    output[output>255] = 255
    output[output<0]   = 0
    output = output.astype(np.uint8)
    return output 

im = cv2.imread('../lenna.bmp', cv2.IMREAD_GRAYSCALE)

kernel0 = np.array([[0, 0, 0], [0, 1, 0], [0, 0, 0]], dtype = float)
im0 = convolve(im, kernel0)
cv2.imwrite('kernel0.png', im0)

kernel1 = np.array([[1, 0, -1], [0, 0, 0], [-1, 0, 1]], dtype = float)
im1 = convolve(im, kernel1)
cv2.imwrite('kernel1.png', im1)

kernel2 = np.array([[0, 1, 0], [1, -4, 1], [0, 1, 0]], dtype = float)
im2 = convolve(im, kernel2)
cv2.imwrite('kernel2.png', im2)

kernel3 = np.array([[-1, -1, -1], [-1, 8, -1], [-1, -1, -1]], dtype = float)
im3 = convolve(im, kernel3)
cv2.imwrite('kernel3.png', im3)

kernel4 = np.array([[0, -1, 0], [-1, 5, -1], [0, -1, 0]], dtype = float)
im4 = convolve(im, kernel4)
cv2.imwrite('kernel4.png', im4)

kernel5 = np.array([[1, 1, 1], [1, 1, 1], [1, 1, 1]], dtype = float)
kernel5 *= 1/9.0
im5 = convolve(im, kernel5)
cv2.imwrite('kernel5.png', im5)

kernel6 = np.array([[1, 2, 1], [2, 4, 2], [1, 2, 1]], dtype = float)
kernel6 *= 1/16.0
im6 = convolve(im, kernel6)
cv2.imwrite('kernel6.png', im6)

kernel7 = np.array([[1, 4, 6, 4, 1], [4, 16, 24, 16, 4], [6, 24, 36, 24, 6], [4, 16, 24, 16, 4], [1, 4, 6, 4, 1]], dtype = float)
kernel7 *= 1/256.0
im7 = convolve(im, kernel7)
cv2.imwrite('kernel7.png', im7)

kernel8 = np.array([[1, 4, 6, 4, 1], [4, 16, 24, 16, 4], [6, 24, -476, 24, 6], [4, 16, 24, 16, 4], [1, 4, 6, 4, 1]], dtype = float)
kernel8 *= (-1/256.0)
im8 = convolve(im, kernel8)
cv2.imwrite('kernel8.png', im8)

#cv2.imshow('image', im8)
#cv2.waitKey(0)
#cv2.destroyAllWindows