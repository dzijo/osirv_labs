import numpy as np
import cv2


def convolve(image, kernel):
    output = np.zeros( (image.shape[0] -kernel.shape[0] + 1,
                        image.shape[1] - kernel.shape[1] +1) )
    kernel_rev = kernel[::-1,::-1]


    for i in range(0, output.shape[0]):
        for j in range(0, output.shape[1]):
            for k in range(kernel.shape[0]):
                for l in range(kernel.shape[1]):
                    output[i,j] += image[i+k,j+l] * kernel_rev[k,l]

    output[output>255] = 255
    output[output<0]   = 0
    output = output.astype(np.uint8)
    return output 


im = cv2.imread('../lenna.bmp', cv2.IMREAD_GRAYSCALE)

#kernel = np.array([[1, 2, 1], [2, 4, 2], [1, 2, 1]], dtype = float)
#kernel *= 1/16.0
kernel = np.array([[1, 4, 6, 4, 1], [4, 16, 24, 16, 4], [6, 24, 36, 24, 6], [4, 16, 24, 16, 4], [1, 4, 6, 4, 1]], dtype = float)
kernel *= 1/256.0
im = convolve(im, kernel)

kernel = np.array([[0, 1, 0], [1, -4, 1], [0, 1, 0]], dtype = float)
im = convolve(im, kernel)

np.clip(im, 10, 255, im)

cv2.imshow('f', im)
cv2.waitKey(0)
cv2.destroyAllWindows()