import numpy as np
import cv2 

im1 = cv2.imread('../lenna.bmp')
im2 = cv2.imread('../airplane.bmp')
im3 = cv2.imread('../pepper.bmp')

#imf = np.concatenate((im1, im2, im3), axis = 1)
imf = np.hstack((im1, im2, im3))

cv2.imwrite('image.png', imf)
cv2.imshow('image', imf)
cv2.waitKey(0)
cv2.destroyAllWindows()