#import cv2
#import numpy as np
#
#def show(img):
#  cv2.imshow("title", img)
#  cv2.moveWindow("title", 800, 200)
#  cv2.waitKey(0)
#  cv2.destroyAllWindows()
#
#img = cv2.imread('../pepper.bmp')
#res = cv2.resize(img,None,fx=0.75, fy=0.25, interpolation = cv2.INTER_LINEAR)
#show(res)

#OR

#height, width = img.shape[:2]
#res = cv2.resize(img,(2*width, 2*height), interpolation = cv2.INTER_LINEAR)
#show(res)

import cv2
import numpy as np
from math import sin, cos, pi

def show(img):
  cv2.imshow("title", img)
  cv2.moveWindow("title", 800, 200)
  cv2.waitKey(0)
  cv2.destroyAllWindows()

img = cv2.imread('../../../images/baboon.bmp', cv2.IMREAD_GRAYSCALE)
rows,cols = img.shape
#img = cv2.resize(img,None,fx=0.50, fy=0.50, interpolation = cv2.INTER_LINEAR)

M = cv2.getRotationMatrix2D((cols/2,rows/2),30,0.25) 
#M = cv2.getRotationMatrix2D((cols/2,rows/2),30,0.75)
#dst = cv2.warpAffine(img,M,(cols,rows))

#img = np.array([[d if d != 0 else c for c, d in zip(a, b)] for a, b in zip(img, img2)])


#show(f)
#img2 = cv2.warpAffine(img,M,(cols,rows))
#M = cv2.getRotationMatrix2D((cols/2,rows/2),30,1)
img2 = np.copy(img)

for i in range(30):
    img2 = cv2.warpAffine(img2,M,(cols,rows))
    img = np.array([[d if d != 0 else c for c, d in zip(a, b)] for a, b in zip(img, img2)])

cv2.imwrite('baboon_sr.png', img)