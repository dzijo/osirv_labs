import numpy as np
import cv2

def func(q, im, noise):
    d = np.power(2, 8 - q)
    im = im.astype(np.float32)
    np.clip(im, 0, 255, im)
    im[:] = (np.floor(im / d + noise) + 0.5) * d
    im = im.astype(np.uint8)
    cv2.imwrite('boats_' + str(q) + 'n.bmp', im)
    #cv2.imshow("i", im)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
    

im = cv2.imread("../../../images/BoatsColor.bmp", cv2.IMREAD_GRAYSCALE)

noise = np.random.uniform(0.0, 1.0, im.shape)

#print(noise)
for i in range(1, 9):
    func(i, im, noise)
