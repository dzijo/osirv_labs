import numpy as np
import cv2
import glob

for filename in glob.glob('../../../images/*'):
    im = cv2.imread(filename, cv2.IMREAD_GRAYSCALE)
    name = filename.split("\\", 1)
    name = name[1].split(".", 1)
    im = cv2.bitwise_not(im)
    cv2.imwrite(name[0] + '_invert.png', im)
    #cv2.imshow(name[0], im)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
