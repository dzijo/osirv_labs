import numpy as np
import cv2
import glob

def func(n):
    for filename in glob.glob('../../../images/*'):
        im = cv2.imread(filename)
        name = filename.split("\\", 1)
        name = name[1].split(".", 1)
        np.clip(im, n, 255, im)
        cv2.imwrite(name[0] + '_' + str(n) + '_thresh.png', im)
        #cv2.imshow(name[0] + '_' + str(n) + '_thresh.png', im)
        #cv2.waitKey(0)
        #cv2.destroyAllWindows()

func(63)
func(127)
func(191)